package com.uhc.aarp.acecs.bookreader.controller;

import com.uhc.aarp.acecs.bookreader.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by palani on 10/17/17.
 */
@RestController
public class BookReaderController {

    @Autowired
    private BookService bookService;

    @Autowired
    private RetryTemplate retryTemplate;

    private String bookstoreUrl = "http://localhost:9080/bookstore/recommended";
    private String retryBookstoreUrl = "http://localhost:9080/bookstore/retry-recommended";

    @RequestMapping("/to-read")
    public String toRead() {
        return bookService.readingList(bookstoreUrl);
    }

    @RequestMapping("/retry-read")
    public String retryRead() {
        return bookService.retryReadingList(retryBookstoreUrl);
    }

    @RequestMapping("/retry-template-read")
    public String retryTemplateRead() {

        return retryTemplate.execute(arg0 -> {
            return bookService.retryTemplateReadingList(retryBookstoreUrl);
        });
    }

    @RequestMapping("/circuit-breaker-read")
    public String circuitBreakerRead() {
        return bookService.circuitBreakerReadingList(retryBookstoreUrl);
    }

}
