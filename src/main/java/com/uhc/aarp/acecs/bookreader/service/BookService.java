package com.uhc.aarp.acecs.bookreader.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.CircuitBreaker;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS;
import static com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager.EXECUTION_TIMEOUT_ENABLED;

/**
 * Created by palani on 10/17/17.
 */
@Slf4j
@Service
public class BookService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RetryTemplate retryTemplate;

    @HystrixCommand(fallbackMethod = "reliable",
            commandProperties = {
                    @HystrixProperty(name = EXECUTION_TIMEOUT_ENABLED, value = "true"),
                    @HystrixProperty(name = EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS, value = "300")
            }
    )
    public String readingList(String url) {
        URI uri = URI.create(url);
        return this.restTemplate.getForObject(uri, String.class);
    }

    public String reliable(String url) {
        return "This is the fallback response from circuit breaker...";
    }

    public String reliable() {
        return "This is the fallback response from circuit breaker...";
    }


    @Retryable(value = {Exception.class}, maxAttempts = 2, backoff = @Backoff(delay = 2000))
    public String retryReadingList(String url) {
        URI uri = URI.create(url);
        return this.restTemplate.getForObject(uri, String.class);
    }

    @Recover
    public String recover(Exception e, String url) {
        log.info("retry failed with exception for the url: {}", url, e);
        return "retry failed... so recover is responding...";
    }

    public String retryTemplateReadingList(String url) {
        URI uri = URI.create(url);
        return this.restTemplate.getForObject(uri, String.class);
    }

    public String readingDefaultUrl() {
        URI uri = URI.create("http://localhost:9080/bookstore/recommended");
        return this.restTemplate.getForObject(uri, String.class);
    }

    @CircuitBreaker(maxAttempts = 3, openTimeout = 2000l, resetTimeout = 60000l)
    public String circuitBreakerReadingList(String url) {
        URI uri = URI.create(url);
        return this.restTemplate.getForObject(uri, String.class);
    }
}
