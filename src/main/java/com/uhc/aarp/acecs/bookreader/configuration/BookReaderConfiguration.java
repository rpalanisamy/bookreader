package com.uhc.aarp.acecs.bookreader.configuration;

import com.uhc.aarp.acecs.bookreader.listener.DefaultListenerSupport;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

/**
 * Created by palani on 10/17/17.
 */
@EnableRetry
//@EnableCircuitBreaker
@Configuration
@ComponentScan(basePackages = {"com.uhc.aarp.acecs.bookreader"})
public class BookReaderConfiguration {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public RetryTemplate retryTemplate() {
        final RetryTemplate retryTemplate = new RetryTemplate();

        final FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(3000l);
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);

        final SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
        simpleRetryPolicy.setMaxAttempts(5);

        retryTemplate.setRetryPolicy(simpleRetryPolicy);

        retryTemplate.registerListener(new DefaultListenerSupport());

        return retryTemplate;
    }

}
